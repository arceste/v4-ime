# V4-ime.el

This is a simple IME for Emacs that allows you to type in the languages of the V4 countries (Hungarian, Polish, Czech, and Slovak).
I originally made it just for Hungarian, but thought that I might as well include the neighbours as well.

To install:<br/>
Put the v4-ime.el in your `.emacs.d/` directory (or wherever you keep your custom scripts), add the loadpath to your Emacs config. For me, this, I used <br/>
`(add-to-list 'load-path "~/.emacs.d/myscripts/")` <br/>

Then load the script with <br/>
`(load "v4-ime.el")`

Load the IME as normal, it will be called `v4-combining`.

## Table of combining characters
Most of the combinations are pretty straightforward, but here's a table because they're not straightforward enough to necessarily guess them all.

| Vowel | Combination |                                                                                                                                                                                        
| ------ | ------ |                                                                                                                                                                                                  
| `Á á` | `A` or `a` + `'` |                                                                                                                                                                                         
| `É é` | `E` or `e` + `'` |                                                                                                                                                                                         
| `Í í` | `I` or `i` + `'` |                                                                                                                                                                                         
| `Ó ó` | `O` or `o` + `'` |                                                                                                                                                                                         
| `Ú ú` | `U` or `u` + `'` |                                                                                                                                                                                         
| `Ý ý` | `Y` or `y` + `'` |                                                                                                                                                                                         
| `Ä ä` | `A` or `a` + `;` |                                                                                                                                                                                         
| `Ô ô` | `O` or `o` + `;` |                                                                                                                                                                                         
| `Ě ě` | `E` or `e` + `;` |                                                                                                                                                                                         
| `Ů ů` | `U` or `u` + `;` |                                                                                                                                                                                         
| `Ą ą` | `A` or `a` + `=` |                                                                                                                                                                                         
| `Ę ę` | `E` or `e` + `=` |                                                                                                                                                                                         
| `Ö ö` | `O` or `o` + `[` |                                                                                                                                                                                         
| `Ü ü` | `U` or `u` + `[` |                                                                                                                                                                                         
| `Ő ő` | `O` or `o` + `]` |                                                                                                                                                                                         
| `Ű ű` | `U` or `u` + `]` |

| Consonant | Combination |                                                                                                                                                                                          
| ----- | ----- |                                                                                                                                                                                                    
| `Ć ć` | `C` or `c` + `'` |                                                                                                                                                                                         
| `Ń ń` | `N` or `n` + `'` |                                                                                                                                                                                         
| `Ś ś` | `S` or `s` + `'` |                                                                                                                                                                                         
| `Ź ź` | `Z` or `z` + `'` |                                                                                                                                                                                         
| `Ĺ ĺ` | `L` or `l` + `'` |                                                                                                                                                                                         
| `Ŕ ŕ` | `R` or `r` + `'` |                                                                                                                                                                                         
| `Č č` | `C` or `c` + `;` |                                                                                                                                                                                         
| `Ď ď` | `D` or `d` + `;` |                                                                                                                                                                                         
| `Ľ ľ` | `L` or `l` + `;` |                                                                                                                                                                                         
| `Ň ň` | `N` or `n` + `;` |                                                                                                                                                                                         
| `Ř ř` | `R` or `r` + `;` |                                                                                                                                                                                         
| `Š š` | `S` or `s` + `;` |                                                                                                                                                                                         
| `Ť ť` | `T` or `t` + `;` |                                                                                                                                                                                         
| `Ž ž` | `Z` or `z` + `;` |                                                                                                                                                                                         
| `Ł ł` | `L` or `l` + `=` |                                                                                                                                                                                         
| `Ż ż` | `Z` or `z` + `=` |
